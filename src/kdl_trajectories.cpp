/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ros/ros.h>
#include <fstream>
#include <sstream>
#include "kdl_trajectories/kdl_trajectories.hpp"


void TrajectoryGenerator::Load(std::string &csv_file_name)
{
  movement_list.clear();
  std::ifstream csv_file(csv_file_name);
  std::string line;
  while (std::getline(csv_file, line, '\n'))
  {
    ROS_INFO_STREAM("line:  " << line);
    std::istringstream iss(line);
    std::string cmd_type;
    if (!(iss >> cmd_type))
    {
      break;
    }
    else
    {
      if (cmd_type.compare("TASKFRAME") == 0)
      {
        double x, y, z, r, p, yaw;
        if (!(iss >> x >> y >> z >> r >> p >> yaw))
        {
          break;
        }
        else
        {
          task_frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          ROS_INFO_STREAM("Setting TASKFRAME: " << task_frame.p << '\t' << task_frame.M);
        }
      }
      else if (cmd_type.compare("LOOP") == 0)
      {
        MovementElement list_element;
        list_element.movetype = cmd_type;
        movement_list.push_back(list_element);
        ROS_INFO_STREAM("Adding LOOP");
      }
      else if (cmd_type.compare("MOVELR") == 0)
      { // TODO: NOT SUPPORTED
        double x, y, z, r, p, yaw, vel;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> vel))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(vel);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVELR frame");
        }
      }
      else if (cmd_type.compare("MOVEL") == 0)
      {
        double x, y, z, r, p, yaw, vel;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> vel))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(vel);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVEL frame");
        }
      }
      else if (cmd_type.compare("MOVELS") == 0)
      { // TODO: NOT SUPPORTED
        double x, y, z, r, p, yaw, dur;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> dur))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(dur);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVELS frame");
        }
      }
      else if (cmd_type.compare("MOVEC") == 0)
      {
        double x, y, z, r, p, yaw, r_end, p_end, yaw_end, px, py, pz, vx, vy, vz, alpha, vel;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> r_end >> p_end >> yaw_end >> px >> py >> pz >> vx >> vy >> vz >> alpha >> vel))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(r_end);
          list_element.options.push_back(p_end);
          list_element.options.push_back(yaw_end);
          list_element.options.push_back(px);
          list_element.options.push_back(py);
          list_element.options.push_back(pz);
          list_element.options.push_back(vx);
          list_element.options.push_back(vy);
          list_element.options.push_back(vz);
          list_element.options.push_back(alpha);
          list_element.options.push_back(vel);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVEC frame");
        }
      }
      else if (cmd_type.compare("MOVECR") == 0)
      {
        double x, y, z, r, p, yaw, r_end, p_end, yaw_end, px, py, pz, vx, vy, vz, alpha, vel, repeat;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> r_end >> p_end >> yaw_end >> px >> py >> pz >> vx >> vy >> vz >> alpha >> vel >> repeat))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(r_end);
          list_element.options.push_back(p_end);
          list_element.options.push_back(yaw_end);
          list_element.options.push_back(px);
          list_element.options.push_back(py);
          list_element.options.push_back(pz);
          list_element.options.push_back(vx);
          list_element.options.push_back(vy);
          list_element.options.push_back(vz);
          list_element.options.push_back(alpha);
          list_element.options.push_back(vel);
          list_element.options.push_back(repeat);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVEC frame");
        }
      }
      else if (cmd_type.compare("WAIT") == 0)
      {
        double w_time;
        if (!(iss >> w_time))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(w_time);
          KDL::Frame frame;
          list_element.frame = frame;
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding WAIT time:  " << w_time);
        }
      }
    }
  }
}

void TrajectoryGenerator::Build(KDL::Frame &X_curr, bool verbose)
{
  ctraject = new KDL::Trajectory_Composite();
  movement_durations.clear();
  polyline.clear();
  t_traj_ = 0.0;

  // Default settings
  double radius = 0.01;
  double eqradius = 0.01;
  double vmax = 0.1; // default value

  // Set frame
  KDL::Frame frame = KDL::Frame(X_curr.M, X_curr.p);
  KDL::Frame new_frame;
  KDL::Frame end_frame;

  // Velocity profile
  KDL::VelocityProfile *velpref;
  KDL::VelocityProfile_Spline *velpref_spline;

  // Paths
  KDL::Path_Line *path_line;
  KDL::Path_Circle *path_circle;
  KDL::Path_Cyclic_Closed *path_cyclic_closed;
  KDL::Path_RoundedComposite *path_roundedcomposite = new KDL::Path_RoundedComposite(radius, eqradius, new KDL::RotationalInterpolation_SingleAxis());

  //------------------------------------
  // Iterate over items in movement_list
  //------------------------------------
  bool on_path = false;
  
  MovementElement new_movement;
  for (int move = 0; move < movement_list.size(); move++)
  {
    new_movement = movement_list[move];
    new_frame = task_frame * KDL::Frame(new_movement.frame.M, new_movement.frame.p);

    //-------------------------
    // MOVELR to path segment
    //-------------------------
    if (in_loop && !on_path)
    {
      if (new_movement.movetype.compare("MOVEL") == 0 ||
          new_movement.movetype.compare("MOVELS") == 0 ||
          new_movement.movetype.compare("MOVEC") == 0 ||
          new_movement.movetype.compare("MOVECR") == 0)
      {
        double vmax_default = 0.05; // default value
        vmax = vmax_default;

        // Line path
        KDL::Frame start_frame = task_frame * KDL::Frame(movement_list[move].frame.M, movement_list[move].frame.p);
        KDL::Path_Line *start_path = new KDL::Path_Line(frame, start_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

        // Add trajectory segment with velocity profile
        velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
        velpref->SetProfile(0, start_path->PathLength()); // Set velocity profile from start and end positions
        ctraject->Add(new KDL::Trajectory_Segment(start_path, velpref));
        init_dur = ctraject->Duration();                   // Save initial time for resetting trajectory
        movement_durations.push_back(velpref->Duration()); // Duration at end of movement
        movement_vmax_old.push_back(vmax);

        if (verbose)
        {
          ROS_INFO_STREAM("Current frame:  " << frame);
          ROS_INFO_STREAM("Add path frame:  " << start_frame);
          ROS_INFO_STREAM("Velocity:  " << vmax);
          ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
          ROS_INFO_STREAM("init_dur: " << init_dur);
          ROS_INFO_STREAM("Path to trajectory finished");
        }
        // Update frame
        frame = KDL::Frame(start_frame.M, start_frame.p);
        on_path = true;
      }
    }

    //------------------------------------------------------
    // LOOP start
    //------------------------------------------------------
    if (new_movement.movetype.compare("LOOP") == 0)
    {
      in_loop = true;
      if (verbose)
      {
        ROS_INFO_STREAM("Start LOOP");
      }
    }
    //------------------------------------------------------
    // MOVEL linear segment (trapezoidal velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVEL") == 0)
    {
      if (!(frame == new_frame))
      {
        // Read movement values
        vmax = new_movement.options[0];

        // Line path
        path_line = new KDL::Path_Line(frame, new_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

        // Add trajectory segment with velocity profile
        velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
        velpref->SetProfile(0, path_line->PathLength()); // Set velocity profile from start and end positions
        ctraject->Add(new KDL::Trajectory_Segment(path_line, velpref));
        movement_durations.push_back(velpref->Duration()); // Duration at end of movement
        movement_vmax_old.push_back(vmax);

        if (verbose)
        {
          ROS_INFO_STREAM("Add MOVEL to frame:  " << new_frame);
          ROS_INFO_STREAM("Velocity:  " << vmax);
          ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
        }

        // Get end frame
        end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));

        // Add line segment to polyline
        polyline.push_back(make_line_segment(new_movement.movetype, frame.p, end_frame.p));

        // Update frame
        frame = KDL::Frame(end_frame.M, end_frame.p);
      }
    }
    //------------------------------------------------------
    // MOVELS linear segment (spline velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVELS") == 0)
    {
      if (!(frame == new_frame))
      {
        // Read movement values
        double duration = new_movement.options[0];

        // Line path
        path_line = new KDL::Path_Line(frame, new_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

        // Add trajectory segment with velocity profile
        velpref_spline = new KDL::VelocityProfile_Spline();
        velpref_spline->SetProfileDuration(0.0, 0.0, 0.0, path_line->PathLength(), 0.0, 0.0, duration);
        ctraject->Add(new KDL::Trajectory_Segment(path_line, velpref_spline));
        movement_durations.push_back(velpref_spline->Duration()); // Duration at end of movement

        if (verbose)
        {
          ROS_INFO_STREAM("Add MOVELS to frame:  " << new_frame);
          ROS_INFO_STREAM("duration:  " << duration);
          ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
        }

        // Get end frame
        end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));

        // Add line segment to polyline
        polyline.push_back(make_line_segment(new_movement.movetype, frame.p, end_frame.p));

        // Update frame
        frame = KDL::Frame(end_frame.M, end_frame.p);
      }
    }
    //------------------------------------------------------
    // MOVEC circular segment (trapezoidal velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVEC") == 0)
    {
      // Read movement values
      KDL::Rotation R_base_end = KDL::Rotation::RPY(new_movement.options[0],
                                                    new_movement.options[1],
                                                    new_movement.options[2]); // Orientation at end of path
      KDL::Vector V_base_center(new_movement.options[3],
                                new_movement.options[4],
                                new_movement.options[5]); // Circle center position
      KDL::Vector V_base_p(new_movement.options[6],
                           new_movement.options[7],
                           new_movement.options[8]); // Circle axis vector
      double alpha = new_movement.options[9];
      vmax = new_movement.options[10];

      // Apply task transformation
      R_base_end = task_frame.M * R_base_end;
      V_base_center = task_frame * V_base_center;
      V_base_p = task_frame * V_base_p;

      // Circle path
      path_circle = new KDL::Path_Circle(frame,
                                         V_base_center,
                                         V_base_p,
                                         R_base_end,
                                         alpha,
                                         new KDL::RotationalInterpolation_SingleAxis(),
                                         eqradius);

      // Add trajectory segment with velocity profile
      velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
      velpref->SetProfile(0, path_circle->PathLength()); // Set velocity profile from start and end positions
      ctraject->Add(new KDL::Trajectory_Segment(path_circle, velpref));
      movement_durations.push_back(velpref->Duration()); // Duration at end of movement
      movement_vmax_old.push_back(vmax);

      // Get end frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));

      if (verbose)
      {
        ROS_INFO_STREAM("Add MOVEC to frame:  " << end_frame);
        ROS_INFO_STREAM("V_base_center:  " << V_base_center);
        ROS_INFO_STREAM("V_base_p:  " << V_base_p);
        ROS_INFO_STREAM("R_base_end:  " << R_base_end);
        ROS_INFO_STREAM("alpha:  " << alpha);
        ROS_INFO_STREAM("Velocity:  " << vmax);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
      }

      // Add line segment to polyline
      for (int seg = 0; seg < NUM_CIRC_SEGS; seg++)
      {
        KDL::Frame circ_frame1 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg) / NUM_CIRC_SEGS));
        KDL::Frame circ_frame2 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg + 1) / NUM_CIRC_SEGS));
        polyline.push_back(make_line_segment(new_movement.movetype, circ_frame1.p, circ_frame2.p));
      }

      // Update frame
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //-------------------------------------------------------------------
    // MOVECR repeatitive circular segment (trapezoidal velocity profile)
    //-------------------------------------------------------------------
    else if (new_movement.movetype.compare("MOVECR") == 0)
    {
      // Read movement values
      KDL::Rotation R_base_end = KDL::Rotation::RPY(new_movement.options[0],
                                                    new_movement.options[1],
                                                    new_movement.options[2]); // Orientation at end of path
      KDL::Vector V_base_center(new_movement.options[3],
                                new_movement.options[4],
                                new_movement.options[5]); // Circle center position
      KDL::Vector V_base_p(new_movement.options[6],
                           new_movement.options[7],
                           new_movement.options[8]); // Circle axis vector
      double alpha = new_movement.options[9];
      vmax = new_movement.options[10];
      int repeat = static_cast<int>(new_movement.options[11]);

      // Cyclic path
      path_cyclic_closed = new KDL::Path_Cyclic_Closed(
          new KDL::Path_Circle(frame,
                               V_base_center,
                               V_base_p,
                               R_base_end,
                               alpha,
                               new KDL::RotationalInterpolation_SingleAxis(),
                               eqradius),
          repeat);

      // Add trajectory segment with velocity profile
      velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
      velpref->SetProfile(0, path_cyclic_closed->PathLength()); // Set velocity profile from start and end positions
      ctraject->Add(new KDL::Trajectory_Segment(path_cyclic_closed, velpref));
      movement_durations.push_back(velpref->Duration()); // Duration at end of movement
      movement_vmax_old.push_back(vmax);

      // Get end frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));

      if (verbose)
      {
        ROS_INFO_STREAM("Add MOVEC to frame:  " << end_frame);
        ROS_INFO_STREAM("V_base_center:  " << V_base_center);
        ROS_INFO_STREAM("V_base_p:  " << V_base_p);
        ROS_INFO_STREAM("R_base_end:  " << R_base_end);
        ROS_INFO_STREAM("alpha:  " << alpha);
        ROS_INFO_STREAM("Velocity:  " << vmax);
        ROS_INFO_STREAM("repeat:  " << repeat);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
      }

      // Add line segment to polyline
      for (int seg = 0; seg < NUM_CIRC_SEGS; seg++)
      {
        KDL::Frame circ_frame1 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg) / NUM_CIRC_SEGS));
        KDL::Frame circ_frame2 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg + 1) / NUM_CIRC_SEGS));
        polyline.push_back(make_line_segment(new_movement.movetype, circ_frame1.p, circ_frame2.p));
      }

      // Update frame
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //------------------------
    // WAIT wait time segment
    //------------------------
    else if (new_movement.movetype.compare("WAIT") == 0)
    {
      // Read movement values
      double wait_time = new_movement.options[0];

      // Add trajectory segment
      ctraject->Add(new KDL::Trajectory_Stationary(wait_time, frame));
      movement_durations.push_back(wait_time); // Duration at end of movement

      if (verbose)
      {
        ROS_INFO_STREAM("Wait time:  " << wait_time);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
        ROS_INFO_STREAM("Wait frame:  " << frame);
      }
    }
    else
    {
      if (verbose)
      {
        ROS_WARN("trajectory movetype not supported");
      }
      exit(EXIT_FAILURE);
    }
  }
  traj_build_ = true;
}

void TrajectoryGenerator::Build(Eigen::Affine3d &X_curr, bool verbose)
{
  KDL::Frame X_curr_kdl;
  tf::transformEigenToKDL(X_curr,X_curr_kdl);
  Build(X_curr_kdl,verbose);
}

void TrajectoryGenerator::Build_vmax(KDL::Frame &X_curr, double vmax, double amax)
{
  // Limit vmax to non-zero
  if (vmax == 0.0)
    vmax = 0.001;

  // Limit amax to non-zero
  if (amax == 0.0)
    amax = 0.001;
  accmax = amax;

  ctraject = new KDL::Trajectory_Composite();

  // Update the duration given the new velocity limits
  int segment_index = 0;
  int segment_counter = 0;
  double cummulative_duration = 0;
  double segment_duration = 0;
  double duration = t_traj_;
  for (int i = 0; i < movement_durations.size(); i++)
  {
    cummulative_duration += movement_durations[i];
    if (duration <= cummulative_duration)
    {
      segment_index = i;
      segment_duration = cummulative_duration - duration;
      break;
    }
  }
  segment_duration = movement_durations[segment_index] - segment_duration;
  movement_durations.clear();

  // Default settings
  double radius = 0.01;
  double eqradius = 0.01;

  // Set frame
  KDL::Frame frame = KDL::Frame(X_curr.M, X_curr.p);
  KDL::Frame new_frame;
  KDL::Frame end_frame;

  // Set velocity profiles
  KDL::VelocityProfile *velpref;
  KDL::VelocityProfile *velpref_old;

  // Paths
  KDL::Path_Line *path_line;
  KDL::Path_Circle *path_circle;
  KDL::Path_Cyclic_Closed *path_cyclic_closed;
  KDL::Path_RoundedComposite *path_roundedcomposite = new KDL::Path_RoundedComposite(radius, eqradius, new KDL::RotationalInterpolation_SingleAxis());

  //------------------------------------
  // Iterate over items in movement_list
  //------------------------------------
  MovementElement new_movement;
  bool on_path = false;
  bool in_loop = false;
  for (int move = 0; move < movement_list.size(); move++)
  {
    new_movement = movement_list[move];
    new_frame = task_frame * KDL::Frame(new_movement.frame.M, new_movement.frame.p);

    //-------------------------
    // MOVELR to path segment
    //-------------------------
    if (in_loop && !on_path)
    {
      if (new_movement.movetype.compare("MOVEL") == 0 ||
          new_movement.movetype.compare("MOVELS") == 0 ||
          new_movement.movetype.compare("MOVEC") == 0 ||
          new_movement.movetype.compare("MOVECR") == 0)
      {
        double vmax_default = 0.1; // default value

        // Line path
        KDL::Frame start_frame = task_frame * KDL::Frame(movement_list[move].frame.M, movement_list[move].frame.p);
        KDL::Path_Line *start_path = new KDL::Path_Line(frame, start_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

        // Add trajectory segment with velocity profile
        velpref = new KDL::VelocityProfile_Trap(std::min(vmax_default, vmax), accmax);
        velpref->SetProfile(0, start_path->PathLength());  // Set velocity profile from start and end positions
        movement_durations.push_back(velpref->Duration()); // Duration at end of movement
        ctraject->Add(new KDL::Trajectory_Segment(start_path, velpref));
        init_dur = ctraject->Duration(); // Save initial time for resetting trajectory
        segment_counter++;

        // Update frame
        frame = KDL::Frame(start_frame.M, start_frame.p);
        on_path = true;
      }
    }

    //------------------------------------------------------
    // LOOP start
    //------------------------------------------------------
    if (new_movement.movetype.compare("LOOP") == 0)
    {
      in_loop = true;
    }
    //------------------------------------------------------
    // MOVEL linear segment (trapezoidal velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVEL") == 0)
    {
      if (!(frame == new_frame))
      {
        // Line path to new_frame
        path_line = new KDL::Path_Line(frame, new_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

        // Add trajectory segment with velocity profile
        vmax = std::min(new_movement.options[0], vmax);
        velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
        velpref->SetProfile(0, path_line->PathLength());   // Set velocity profile from start and end positions
        movement_durations.push_back(velpref->Duration()); // Duration at end of movement
        ctraject->Add(new KDL::Trajectory_Segment(path_line, velpref));

        // Update segment_duration for modified velocity
        if (segment_index == segment_counter)
        {
          velpref_old = new KDL::VelocityProfile_Trap(movement_vmax_old[segment_counter], accmax);
          velpref_old->SetProfile(0, path_line->PathLength()); // Set velocity profile from start and end positions
          segment_duration = compute_time_vmax(velpref_old->Pos(segment_duration),
                                               velpref_old->Pos(velpref_old->Duration()),
                                               velpref_old->Duration(),
                                               velpref->Duration(),
                                               vmax,
                                               accmax);
        }
        movement_vmax_old[segment_counter] = vmax;
        segment_counter++;

        // Get end frame
        end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));

        // Add line segment to polyline
        polyline.push_back(make_line_segment(new_movement.movetype, frame.p, end_frame.p));

        // Update frame
        frame = KDL::Frame(end_frame.M, end_frame.p);
      }
    }
    //------------------------------------------------------
    // MOVEC circular segment (trapezoidal velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVEC") == 0)
    {
      // Read movement values
      KDL::Rotation R_base_end = KDL::Rotation::RPY(new_movement.options[0],
                                                    new_movement.options[1],
                                                    new_movement.options[2]); // Orientation at end of path
      KDL::Vector V_base_center(new_movement.options[3],
                                new_movement.options[4],
                                new_movement.options[5]); // Circle center position
      KDL::Vector V_base_p(new_movement.options[6],
                           new_movement.options[7],
                           new_movement.options[8]); // Circle axis vector
      double alpha = new_movement.options[9];
      vmax = std::min(new_movement.options[10], vmax);

      // Apply task transformation
      R_base_end = task_frame.M * R_base_end;
      V_base_center = task_frame * V_base_center;
      V_base_p = task_frame * V_base_p;

      // Circle path
      path_circle = new KDL::Path_Circle(frame,
                                         V_base_center,
                                         V_base_p,
                                         R_base_end,
                                         alpha,
                                         new KDL::RotationalInterpolation_SingleAxis(),
                                         eqradius);

      // Add trajectory segment with velocity profile
      velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
      velpref->SetProfile(0, path_circle->PathLength()); // Set velocity profile from start and end positions
      movement_durations.push_back(velpref->Duration()); // Duration at end of movement
      ctraject->Add(new KDL::Trajectory_Segment(path_circle, velpref));

      // Update segment_duration for modified velocity
      if (segment_index == segment_counter)
      {
        velpref_old = new KDL::VelocityProfile_Trap(movement_vmax_old[segment_counter], accmax);
        velpref_old->SetProfile(0, path_circle->PathLength()); // Set velocity profile from start and end positions
        segment_duration = compute_time_vmax(velpref_old->Pos(segment_duration),
                                             velpref_old->Pos(velpref_old->Duration()),
                                             velpref_old->Duration(),
                                             velpref->Duration(),
                                             vmax,
                                             accmax);
      }
      movement_vmax_old[segment_counter] = vmax;
      segment_counter++;

      // Get end frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));

      // Add line segment to polyline
      for (int seg = 0; seg < NUM_CIRC_SEGS; seg++)
      {
        KDL::Frame circ_frame1 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg) / NUM_CIRC_SEGS));
        KDL::Frame circ_frame2 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg + 1) / NUM_CIRC_SEGS));
        polyline.push_back(make_line_segment(new_movement.movetype, circ_frame1.p, circ_frame2.p));
      }

      // Update frame
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //-------------------------------------------------------------------
    // MOVECR repeatitive circular segment (trapezoidal velocity profile)
    //-------------------------------------------------------------------
    else if (new_movement.movetype.compare("MOVECR") == 0)
    {
      // Read movement values
      KDL::Rotation R_base_end = KDL::Rotation::RPY(new_movement.options[0],
                                                    new_movement.options[1],
                                                    new_movement.options[2]); // Orientation at end of path
      KDL::Vector V_base_center(new_movement.options[3],
                                new_movement.options[4],
                                new_movement.options[5]); // Circle center position
      KDL::Vector V_base_p(new_movement.options[6],
                           new_movement.options[7],
                           new_movement.options[8]); // Circle axis vector
      double alpha = new_movement.options[9];
      vmax = std::min(new_movement.options[10], vmax);
      int repeat = static_cast<int>(new_movement.options[11]);

      // Cyclic path
      path_cyclic_closed = new KDL::Path_Cyclic_Closed(
          new KDL::Path_Circle(frame,
                               V_base_center,
                               V_base_p,
                               R_base_end,
                               alpha,
                               new KDL::RotationalInterpolation_SingleAxis(),
                               eqradius),
          repeat);

      // Add trajectory segment with velocity profile
      velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
      velpref->SetProfile(0, path_cyclic_closed->PathLength()); // Set velocity profile from start and end positions
      movement_durations.push_back(velpref->Duration());        // Duration at end of movement
      ctraject->Add(new KDL::Trajectory_Segment(path_cyclic_closed, velpref));

      // Update segment_duration for modified velocity
      if (segment_index == segment_counter)
      {
        velpref_old = new KDL::VelocityProfile_Trap(movement_vmax_old[segment_counter], accmax);
        velpref_old->SetProfile(0, path_cyclic_closed->PathLength()); // Set velocity profile from start and end positions
        segment_duration = compute_time_vmax(velpref_old->Pos(segment_duration),
                                             velpref_old->Pos(velpref_old->Duration()),
                                             velpref_old->Duration(),
                                             velpref->Duration(),
                                             vmax,
                                             accmax);
      }
      movement_vmax_old[segment_counter] = vmax;
      segment_counter++;

      // Get end frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));

      // Add line segment to polyline
      for (int seg = 0; seg < NUM_CIRC_SEGS; seg++)
      {
        KDL::Frame circ_frame1 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg) / NUM_CIRC_SEGS));
        KDL::Frame circ_frame2 = KDL::Frame(ctraject->Pos((ctraject->Duration() - velpref->Duration()) + velpref->Duration() * static_cast<double>(seg + 1) / NUM_CIRC_SEGS));
        polyline.push_back(make_line_segment(new_movement.movetype, circ_frame1.p, circ_frame2.p));
      }

      // Update frame
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //------------------------
    // WAIT wait time segment
    //------------------------
    else if (new_movement.movetype.compare("WAIT") == 0)
    {
      // Read movement values
      double wait_time = new_movement.options[0];

      // Add trajectory segment
      movement_durations.push_back(wait_time); // Duration at end of movement
      ctraject->Add(new KDL::Trajectory_Stationary(wait_time, frame));
      segment_counter++;
    }
  }

  // Update the duration given the new velocity limits
  cummulative_duration = 0;
  for (int i = 0; i < segment_index; i++)
  {
    cummulative_duration += movement_durations[i];
  }
  t_traj_ = cummulative_duration + segment_duration;
}

double TrajectoryGenerator::compute_time_vmax(double Pos, double Pos_length, double duration_old, double duration_new, double maxvel_new, double maxacc_new)
{
  // Determine new coefficients
  double t1, t2, duration;
  t1 = maxvel_new / maxacc_new;
  double s = KDL::sign(Pos_length);
  double deltax1 = s * maxacc_new * KDL::sqr(t1) / 2.0;
  double deltaT = (Pos_length - 2.0 * deltax1) / (s * maxvel_new);
  if (deltaT > 0.0)
  {
    // plan a complete profile :
    duration = 2.0 * t1 + deltaT;
    t2 = duration - t1;
  }
  else
  {
    // plan an incomplete profile :
    t1 = KDL::sqrt((Pos_length) / s / maxacc_new);
    duration = t1 * 2.0;
    t2 = t1;
  }

  // Solve for new time
  if (Pos <= 0.0)
  {
    return 0.0;
  }
  else if (Pos >= duration_old)
  {
    return duration_new;
  }

  // Solver for time
  //     double Pos_ramp_limit = t1 * maxvel_new / 2.0;
  double Pos_ramp_limit = KDL::sqr(t1) * maxacc_new / 2.0;
  double Pos_linear_limit = (t2 - t1) * maxvel_new;
  if (Pos < Pos_ramp_limit)
  { // in ramp up
    return KDL::sqrt((2.0 * Pos) / maxacc_new);
  }
  else if (Pos < (Pos_ramp_limit + Pos_linear_limit))
  { // in linear
    return (Pos - Pos_ramp_limit) / maxvel_new + t1;
  }
  else
  { // in ramp down
    return duration_new - KDL::sqrt((2.0 * (Pos_length - Pos)) / maxacc_new);
  }
}

LineSegment TrajectoryGenerator::make_line_segment(std::string movetype, KDL::Vector v1, KDL::Vector v2)
{
  LineSegment line_segment;
  line_segment.movetype = movetype;
  line_segment.start_point = v1;
  line_segment.end_point = v2;
  return line_segment;
}

double TrajectoryGenerator::Duration()
{
  return ctraject->Duration();
}

double TrajectoryGenerator::ResetDuration()
{
  return init_dur;
}

Eigen::Affine3d TrajectoryGenerator::Pos()
{
  Eigen::Affine3d X_traj;
  tf::transformKDLToEigen(X_traj_,X_traj);

  return X_traj;
}

Eigen::Matrix<double,6,1> TrajectoryGenerator::Vel()
{
  Eigen::Matrix<double,6,1>  Xd_traj;
  tf::twistKDLToEigen(Xd_traj_,Xd_traj);

  return Xd_traj;
}

Eigen::Matrix<double,6,1> TrajectoryGenerator::Acc()
{ 
  Eigen::Matrix<double,6,1>  Xdd_traj;
  tf::twistKDLToEigen(Xdd_traj_,Xdd_traj);

  return Xdd_traj;
}

void TrajectoryGenerator::SetAccMax(double amax)
{
  accmax = amax;
}

Eigen::Vector3d TrajectoryGenerator::DirectionOfMotion(double time, double dt)
{
  KDL::Frame X_traj_ = ctraject->Pos(time);           // Get next point along the trajectory
  KDL::Frame X_traj_next_ = ctraject->Pos(time + dt); // Get next point along the trajectory
  if (X_traj_next_ != X_traj_)
  {
    KDL::Vector dir_ = KDL::diff(X_traj_next_.p, X_traj_.p);
    double dir_n = dir_.Normalize();
    if (dir_n != 0)
    {
      for (int i = 0; i < 3; ++i)
        u(i) = dir_(i);
    }
  } // Else keep the previous direction
  // If needed we can filter the direction to smooth it

  return u;
}

void TrajectoryGenerator::trajectory_progress(KDL::Frame &X_curr){
    // Get ring plane equation (n_ring = (a,b,c), p_ring = (x,y,z), d_ = n_ring*p_ring)
    KDL::Vector p_ring = X_curr_.p;
    KDL::Vector n_ring = X_curr_.M.UnitX();

    // Intersect ring plane with trajectory polyline
    double total_length = 0;
    intersection_points.clear();
    std::vector<double> total_length_at_intersection_points;
    for (int seg=0; seg<polyline.size(); seg++){
        KDL::Vector seg_w = p_ring - polyline[seg].start_point;
        KDL::Vector seg_u = polyline[seg].end_point - polyline[seg].start_point;
        double s_inter = (dot(seg_w, n_ring) / dot(seg_u, n_ring));        
        // Intersection along segment is valid if s_inter in [0,1]
        if (s_inter >= 0 && s_inter <= 1){
            KDL::Vector p_inter = polyline[seg].start_point + s_inter * seg_u;
            intersection_points.push_back(p_inter);

            // Increment total_length
            double total_length_at_intersection_point = total_length + s_inter * seg_u.Norm();
            total_length_at_intersection_points.push_back(total_length_at_intersection_point);
        }
        total_length += seg_u.Norm();
    }

    // Find closest point in intersection_points
    double min_distance = 100;
    for (int pt=0; pt<intersection_points.size(); pt++){
        double dist_p_inter = (intersection_points[pt] - p_ring).Norm();
        if (dist_p_inter < min_distance){
            min_distance = dist_p_inter;
            wireloop_point = intersection_points[pt];

            // Get progress along trajectory
            double progress = total_length_at_intersection_points[pt] / total_length;
            if (progress <= 0.5)
              percent_completion = 200 * progress;
        }
    }
}

kdl_trajectories::PublishTraj TrajectoryGenerator::publishTrajectory()
{
  kdl_trajectories::PublishTraj publish_traj_;
  // publish trajectory
  nav_msgs::Path path_ros;
  geometry_msgs::PoseArray pose_array;
  geometry_msgs::PoseStamped pose_st;
  for (double t = 0.0; t <= ctraject->Duration(); t += ctraject->Duration() / 50.0) // TODO check if ok
  {
    KDL::Frame current_pose;
    current_pose = ctraject->Pos(t);
    geometry_msgs::Pose pose;

    tf::poseKDLToMsg(current_pose, pose);
    pose_array.poses.push_back(pose);
    pose_st.pose = pose;
    path_ros.poses.push_back(pose_st);
  }

  publish_traj_.pose_array_ = pose_array;
  publish_traj_.path_ros_ = path_ros;

  return publish_traj_;
}

double TrajectoryGenerator::CurrentTime(){
  return t_traj_;
}

void TrajectoryGenerator::previous(kdl_trajectories::TrajProperties traj_properties_, double time_dt_)
{
  if (traj_properties_.play_traj_)
  {
    t_traj_ -= time_dt_;
    X_traj_ = ctraject->Pos(t_traj_);
    Xd_traj_ = ctraject->Vel(t_traj_);
    Xdd_traj_ = ctraject->Acc(t_traj_);
  }
}
void TrajectoryGenerator::updateTrajectory(kdl_trajectories::TrajProperties traj_properties_, double time_dt_)
{
  tf::poseMsgToKDL(traj_properties_.X_curr_, X_curr_);
  tf::poseMsgToKDL(traj_properties_.X_des_jog_, X_des_jog_);
  if (traj_build_)
  {
    trajectory_progress(X_curr_);
    if (traj_properties_.play_traj_)
    {
      t_traj_ += time_dt_;
      if (t_traj_ > ctraject->Duration() && in_loop)
      {
        t_traj_ = init_dur;
      }
      X_traj_ = ctraject->Pos(t_traj_);
      Xd_traj_ = ctraject->Vel(t_traj_);
      Xdd_traj_ = ctraject->Acc(t_traj_);
    }
    else if (traj_properties_.jogging_)
    {
      X_traj_ = X_des_jog_;
      Xd_traj_.Zero();
      Xdd_traj_.Zero();
    }
    else if (traj_properties_.gain_tunning_)
    {
      if (traj_properties_.move_)
      { 
        index_ = traj_properties_.index_;
        std::cout << "Moving index " << index_ << "with an amplitude of " << traj_properties_.amplitude << std::endl;
        if (toggle_)
        {
          if (index_ < 3)
          {
            std::cout << "X_curr_" << X_curr_.p(0) << "; " << X_curr_.p(1) << ";" <<  X_curr_.p(2) << std::endl; 
            X_traj_.p(index_) = X_curr_.p(index_) + traj_properties_.amplitude;
          }
          else if (index_ == 3)
            X_traj_.M = KDL::Rotation::RotX(traj_properties_.amplitude * M_PI / 180.0) * X_curr_.M;
          else if (index_ == 4)
            X_traj_.M = KDL::Rotation::RotY(traj_properties_.amplitude * M_PI / 180.0) * X_curr_.M;
          else if (index_ == 5)
            X_traj_.M = KDL::Rotation::RotZ(traj_properties_.amplitude * M_PI / 180.0) * X_curr_.M;
        }
        else
        {
          if (index_ < 3)
          {
            std::cout << "X_curr_" << X_curr_.p(0) << "; " << X_curr_.p(1) << ";" <<  X_curr_.p(2) << std::endl; 
            X_traj_.p(index_) = X_curr_.p(index_) - traj_properties_.amplitude;
          }
          else if (index_ == 3)
            X_traj_.M = KDL::Rotation::RotX(-traj_properties_.amplitude * M_PI / 180.0) * X_curr_.M;
          else if (index_ == 4)
            X_traj_.M = KDL::Rotation::RotY(-traj_properties_.amplitude * M_PI / 180.0) * X_curr_.M;
          else if (index_ == 5)
            X_traj_.M = KDL::Rotation::RotZ(-traj_properties_.amplitude * M_PI / 180.0) * X_curr_.M;
        }
        toggle_ = !toggle_;
        Xd_traj_.Zero();
        Xdd_traj_.Zero();
      }
    }
    else
    {
      X_traj_ = ctraject->Pos(t_traj_);
      Xd_traj_.Zero();
      Xdd_traj_.Zero();
    }
  }
  else
  {
    X_traj_ = X_curr_;
    Xd_traj_.Zero();
    Xdd_traj_.Zero();
  }
}
